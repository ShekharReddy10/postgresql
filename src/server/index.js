const path = require("path");
const fsp = require("fs/promises");

const { Client } = require("pg");
const csvtojson = require("csvtojson");
const format = require("pg-format");

const csvFilePathForMatches = path.join(__dirname, '../data/matches.csv');
const csvFilePathForDeliveries = path.join(__dirname, '../data/deliveries.csv');
const client = new Client("postgres://eliotdeh:3CHrfKj_58MQTD_KtYC_kG8gOhNPvbka@floppy.db.elephantsql.com/eliotdeh");

client
    .connect()
    .then(() => {
        return client.query(createTableForMatchesQuery());
    })
    .then(() => {
        console.log("Matches Table is created.");
        return csvtojson().fromFile(csvFilePathForMatches);
    })
    .then((matches) => {
        console.log("Matches.CSV converted to JSON format.");
        const insertQuery = `INSERT INTO matches (
            season ,
            city ,
            date ,
            team1 ,
            team2 ,
            toss_winner ,
            toss_decision ,
            result ,
            dl_applied ,
            winner ,
            win_by_runs ,
            win_by_wickets ,
            player_of_match ,
            venue ,
            umpire1 ,
            umpire2 ,
            umpire3 ) VALUES %L`;
        const values = matches.map((match) => {
            return Object.values(match).slice(1);
        });
        return client.query(format(insertQuery, values));
    })
    .then(() => {
        console.log("Matches Table insertions are done.");
        return client.query(createTableForDeliveriesQuery());
    })
    .then(() => {
        console.log("Deliveries Table is created.");
        return csvtojson().fromFile(csvFilePathForDeliveries);
    })
    .then((deliveries) => {
        console.log("Deliveries.CSV converted to JSON format.");
        const insertQuery = `INSERT INTO deliveries (
            match_id,
            inning,
            batting_team,
            bowling_team,
            over,
            ball,
            batsman,
            non_striker,
            bowler,
            is_super_over,
            wide_runs,
            bye_runs, 
            legbye_runs,
            noball_runs,
            penalty_runs,
            batsman_runs,
            extra_runs,
            total_runs,
            player_dismissed,
            dismissal_kind,
            fielder) VALUES %L`;
        const values = deliveries.map((delivery) => {
            return Object.values(delivery);
        });
        let insertALL = [];
        let noOfRows = 0;
        let totalRows = values.length;
        while (noOfRows <= values.length) {
            if (totalRows < 100000) {
                insertALL.push(client.query(format(insertQuery, values.slice(noOfRows, values.length + 1))))
                noOfRows = values.length + 1;
            } else {
                insertALL.push(client.query(format(insertQuery, values.slice(noOfRows, noOfRows + 100000 + 1))))
                noOfRows = noOfRows + 100000 + 1;
                totalRows = totalRows - 100000 + 1;
            }
        }
        return Promise.all(insertALL);
    })
    .then(() => {
        console.log("Deleveries Table insertions are done.");
        return client.query(`select season , count(*) as Total_matches from matches group by season order by season;`);
    })
    .then((matchesPlayedPerSeason) => {
        console.log("Query returned successfully for Number of Matches Played Per Season ");
        return fsp.writeFile(path.join(__dirname, '../output/numberOfMatchesPlayedPerSeason.json'), JSON.stringify(matchesPlayedPerSeason.rows));
    })
    .then(() => {
        console.log("numberOfMatchesPlayedPerSeason.json file created and written output to it.");
        return client.query(`select season , winner, count(*) as Number_of_wins from matches group by season, winner order by season;`);
    })
    .then((matchesWonPerTeamPerYearResponse) => {
        console.log("Query returned successfully for Number of Matches Won Per Team Per Year");
        let matchesWonPerTeamPerYear = matchesWonPerTeamPerYearResponse.rows.reduce((acc, response) => {
            let year = response.season;
            let team = response.winner;
            let wins = response.number_of_wins;
            if (acc[year] === undefined) {
                acc[year] = { [team]: wins };
            } else {
                acc[year][team] = wins;
            }
            return acc;
        }, {})
        return fsp.writeFile(path.join(__dirname, '../output/matchesWonPerTeamPerYear.json'), JSON.stringify(matchesWonPerTeamPerYear));
    })
    .then(() => {
        console.log("matchesWonPerTeamPerYear.json file created and written output to it.");
        let season = 2016;
        return client.query(`select bowling_team, sum(cast(extra_runs as INTEGER)) as Total_extra_runs from deliveries 
        join matches on match_id = id where season = ${season}
        group by bowling_team;`);
    })
    .then((ExtraRunsPerTeam) => {
        console.log("Query returned successfully for Extra Runs Conceaded Per Team");
        return fsp.writeFile(path.join(__dirname, '../output/extraRunsConceadedPerTeam.json'), JSON.stringify(ExtraRunsPerTeam.rows));
    })
    .then(() => {
        console.log("extraRunsConceadedPerTeam.json file created and written output to it.");
        let season = 2015;
        return client.query(`select bowler, (sum(total_runs)/( count(*) filter(where wide_runs = 0 AND noball_runs = 0)/6:: float)::float) as economy 
        from deliveries join matches on match_id = id where season = ${season}
        group by bowler
        order by economy limit 10;`)
    })
    .then((top10EconomicalBowlers) => {
        console.log("Query returned successfully for Top-10-Economical-Bowlers");
        return fsp.writeFile(path.join(__dirname, '../output/top10EconomicalBowlers.json'), JSON.stringify(top10EconomicalBowlers.rows));
    })
    .then(() => {
        console.log("top10EconomicalBowlers.json file created and written output to it.");
        client.end();
    })
    .catch((err) => {
        console.error("Failed", err);
    })

function createTableForDeliveriesQuery() {
    return `DROP TABLE IF EXISTS deliveries;
    CREATE TABLE deliveries(
        match_id INTEGER,
        inning INTEGER,
        batting_team VARCHAR(255),
        bowling_team VARCHAR(255),
        over INTEGER,
        ball INTEGER,
        batsman VARCHAR(255),
        non_striker VARCHAR(255),
        bowler VARCHAR(255),
        is_super_over INTEGER,
        wide_runs INTEGER,
        bye_runs INTEGER,
        legbye_runs INTEGER,
        noball_runs INTEGER,
        penalty_runs INTEGER,
        batsman_runs INTEGER,
        extra_runs INTEGER,
        total_runs INTEGER,
        player_dismissed VARCHAR(255),
        dismissal_kind VARCHAR(255),
        fielder VARCHAR(255)
    );`
}

function createTableForMatchesQuery() {
    return `DROP TABLE IF EXISTS matches;
    CREATE TABLE matches (
          id SERIAL PRIMARY KEY,
          season INTEGER NOT NULL,
          city VARCHAR(255) NOT NULL,
          date VARCHAR(255),
          team1 VARCHAR(255),
          team2 VARCHAR(255),
          toss_winner VARCHAR(255),
          toss_decision VARCHAR(255),
          result VARCHAR(255),
          dl_applied VARCHAR(255),
          winner VARCHAR(255),
          win_by_runs INTEGER,
          win_by_wickets INTEGER,
          player_of_match VARCHAR(255),
          venue VARCHAR(255),
          umpire1 VARCHAR(255),
          umpire2 VARCHAR(255),
          umpire3 VARCHAR(255)
      );`;
}